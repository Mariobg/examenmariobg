package com.mario.examen.service;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.mario.examen.converter.UsuarioConverter;
import com.mario.examen.entity.Usuario;
import com.mario.examen.model.UsuarioModel;
import com.mario.examen.repository.UsuarioRepository;

@Service("usuarioService")
public class UsuarioService {
	
	@Autowired
	@Qualifier("usuarioRepository")
	private UsuarioRepository repository;
	
	@Autowired
	@Qualifier("usuarioConvert")
	private UsuarioConverter convert;
	
	private static final Logger logger = LoggerFactory.getLogger(UsuarioService.class);
	
	public boolean crear(Usuario usuario) {
		logger.info("Vamos a crear un usuario.");
		try {
			repository.save(usuario);
			logger.info("El usuario  asido creada correctamente.");
			return true;
		}catch(Exception e) {
			logger.error("Error al crear el usuario");
			logger.error(e.getMessage());
			return false;
		}
	}
	
	public boolean editar(Usuario usuario) {
		 logger.info("Vamos a editar un usuario.");
		try {
			repository.save(usuario);
			logger.info("El usuario  asido editado correctamente.");
			return true;
		}catch(Exception e) {
			logger.error("Error al editar el usuario");
			return false;
		}
	}
	
	public boolean eliminar(long id) {
		 logger.info("Vamos a eliminar un usuario.");
		try {
			Usuario usuario = repository.findById( id);
			repository.delete(usuario);
			logger.info("El usuario  asido eliminado correctamente.");
			return true;
		}catch(Exception e) {
			logger.error("Error al eliminar el usuario");
			return false;
		}
	}
	
	public List<UsuarioModel> listar(){
		 logger.info("listar usuarios.");
		return  convert.convertirLista(repository.findAll());
	} 
	
	public UsuarioModel obtenerPorId(long id) {
		 logger.info("obtener usuarios por id.");
		 return convert.convertirUsuario(repository.findById(id));
	}
	
	public UsuarioModel obtenerPorNombre(String nombre) {
		 logger.info("obtener usuarios por nombre.");
		return convert.convertirUsuario(repository.findByNombre(nombre));
	}

}
