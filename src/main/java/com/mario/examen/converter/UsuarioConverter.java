package com.mario.examen.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.mario.examen.entity.Usuario;
import com.mario.examen.entity.Rol;
import com.mario.examen.model.RolModel;
import com.mario.examen.model.UsuarioModel;

@Component("usuarioConvert")
public class UsuarioConverter {

	public List<UsuarioModel> convertirLista(List<Usuario> usuarios){
		List<UsuarioModel>  mUsuarios = new ArrayList<>();
		for(Usuario usuario:usuarios) {
			UsuarioModel mUsuario = this.convertirUsuario(usuario);
			mUsuarios.add(mUsuario);
		}
		return mUsuarios;
	}
	
	public UsuarioModel convertirUsuario(Usuario usuario){
		UsuarioModel mUsuario=new UsuarioModel();
		mUsuario.setId(usuario.getId());
		mUsuario.setNombre(usuario.getNombre());
		mUsuario.setApellidoPaterno(usuario.getApellidoPaterno());
		mUsuario.setApellidoMaterno(usuario.getApellidoMaterno());
		List<RolModel> roles = new ArrayList<>();
		for(Rol rol:usuario.getRoles()){
			RolModel rolModel=new RolModel();
			rolModel.setId(rol.getId());
			rolModel.setNombre(rol.getNombre());
			roles.add(rolModel);
		}
		mUsuario.setRoles(roles);
		return mUsuario;
	}
	
}
