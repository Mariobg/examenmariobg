package com.mario.examen.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mario.examen.entity.Usuario;
import com.mario.examen.model.UsuarioModel;
import com.mario.examen.service.UsuarioService;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {
	
	@Autowired
	@Qualifier("usuarioService")
	private UsuarioService service;
	
	@PostMapping
	public boolean save(@RequestBody Usuario usuario) {
		return service.crear(usuario);
	}
	
	@PutMapping
	public boolean update(@RequestBody  Usuario usuario) {
		return service.editar(usuario);
	}

	@DeleteMapping("/{id}")
	public boolean delete(@PathVariable("id") long id) {
		return service.eliminar(id);
	}
	
	@GetMapping
	public List<UsuarioModel> get(){
		return service.listar();
	}
	
	@GetMapping("/{id}")
	public UsuarioModel obtenerPorId(@PathVariable("id") long id){
		return service.obtenerPorId(id);
	}
	
	@GetMapping("/nombre/{nombre}")
	public UsuarioModel obtenerPorNombre(@PathVariable("nombre") String nombre){
		return service.obtenerPorNombre(nombre);
	}

}
