package com.mario.examen.model;

import java.util.List;

import com.mario.examen.entity.Rol;

public class RolModel {
	
	private long id;
	private String nombre;
	
	public RolModel() {
		
	}
	
	public RolModel(Rol rol) {
		this.id = rol.getId();
		this.nombre = rol.getNombre();
	}
	
	public RolModel(long id, String nombre, List<UsuarioModel> usuarios) {
		this.id = id;
		this.nombre = nombre;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
