package com.mario.examen.model;

import java.util.Collection;
import java.util.List;

public class UsuarioModel {
	
	private long id;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private List<RolModel> roles;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public Collection<RolModel> getRoles() {
		return roles;
	}
	public void setRoles(List<RolModel> roles) {
		this.roles = roles;
	}
	
}
