package com.mario.examen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mario.examen.entity.Usuario;


@Repository("usuarioRepository")
public interface UsuarioRepository extends JpaRepository<Usuario,Serializable> {
	 
	public abstract Usuario findById(long  id);
	public abstract Usuario findByNombre(String nombre);
	 
}
