package com.mario.examen.repository;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mario.examen.entity.Rol;

@Repository("rolRepository")
public interface RolRepository extends JpaRepository<Rol,Serializable> {
	public abstract Rol findById(long  id);
}
