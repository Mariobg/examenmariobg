package com.mario.examen.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Table(name="tusuario")
@Entity
public class Usuario  implements Serializable {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name="id")
	private long id;
	
	@Column(name="nombre",length=50)
	private String nombre;
	
	@Column(name="a_paterno",length=50)
	private String apellidoPaterno;
	
	@Column(name="a_materno",length=50)
	private String apellidoMaterno;
	
	@ManyToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	
	private List<Rol> roles;
	
	public Usuario() {
		
	}
	
	public Usuario(Usuario usuario ) {
		
		this.id = usuario.getId();
		this.nombre = usuario.getNombre();
		this.apellidoPaterno = usuario.getApellidoPaterno();
		this.apellidoMaterno = usuario.getApellidoMaterno();
		this.roles = usuario.getRoles();
	}

	public Usuario(long id, String nombre, String apellidoPaterno, String apellidoMaterno, List<Rol> roles) {
		this.id = id;
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.roles = roles;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return this.apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public List<Rol> getRoles() {
		return roles;
	}

	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}

}
